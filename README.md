# Kubernetes_project1

# A Step-by-Step Guide to Migrating from Local Machine to Kubernetes in the Cloud

# Steps: 
1. Run a program in your local machine
1.  Containerize the program 
1. Run the container in your local machine
1. Deploy the container to cloud (AWS) 
1. Expose the container to the world 
1. Scale the container


# Prerequisites: 
1. Install python:
 https://www.python.org/downloads/
1. Install Flask:
 https://flask.palletsprojects.com/en/2.2.x/installation/
1. Install Docker GUI:
 https://docs.docker.com/desktop/
1. AWS Account 
1. Install aws-iam-authenticator:
 https://docs.aws.amazon.com/eks/latest/userguide/1.1.           
1. Install eksctl:
https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html
1. Install kubectl:
https://docs.aws.amazon.com/eks/





# Let’s get started 

## Run a program in your local machine
Write a simple python code to import Flask (By default flask applications run on port 5000, but you can use any port, I used port 5500). 

# Install flask on your PWD: run the following commands 
- Python3 -m venv venv →  to create an environment
- . venv/bin/activate        →  to activate the environment 
- Pip install flask             → to install flask  
 

# Check if the server is running 

- Run python3 ./webserver.py → to check if the server is running on the specified port
- Verify on your browser with → localhost:5500

## Containerize the Code 
Create a Dockerfile and dependencies

# Run the following commands to create an image: 
- docker build - t <image_name> . → to create an image 
- docker images → to view all images 



# Run the following commands to create a container from the image:
- docker run -d -p 5500:5500 <image_name> → to create a container 
- docker ps → to view all running containers 
- docker logs <container_id> → to see the logs in the container running


## Prepare for the Cloud 
Create a repository in docker hub 

# Run the following commands from your CLI:
- docker login → to login to docker hub from your CLI 
- docker tag <image_id> <repo_name>:version → to tag the image 
- docker push <repo_name>:version → to push to docker hub repository

## Deploy into Cloud (AWS) 
# Create an eks cluster - you can use the  AWS console, IAC tool such as terraform, or with a single eksctl command

- eksctl create cluster –name <cluster_name> –fargate → to create an eks cluster in AWS 
- kubectl get pods → to see running pods

# Create a Kubernetes manifest for both deployment and service

# Run the following commands:
- kubectl apply -f deployment.yaml → to deploy the manifest 
- Kubectl get pods → to see running pods 
- Kubectl get service → to see the svc created with the LoadBa
lancer

# Expose the container to the world by pasting the LoadBalancer external IP to your browser. 

# You can scale the container by increasing the replica to any value and re-apply the manifest. 

# What’s Next?
- Logging and Monitoring
- Multiple Apps in cluster - Namespaces 
- HPA, Cluster Autoscaler
- DevOps
- Ingress 
- Best Practices - Security, Cost optimization, etc.

